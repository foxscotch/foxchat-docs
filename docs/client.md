# Chat Client

This document describes what is expected of the client. Specifically the HTML
client that's part of the project, however, the first part will also describe
what any other client would need to do.



## How a client interacts with the server

This stuff applies to any client, and MUST be followed in order for a client to
properly use a Foxchat server. First up, let's go over how you authenticate
yourself with the client.


### Authentication

The user should log in through the [web interface](web.md). I guess technically
this is _all_ a "web interface," but I need to differentiate between the HTTP
traffic and the chat traffic, and "web" and "chat," respectively, achieve that.

The user will get their session ID when they [log in](web.md#login), and the
client needs to send that session ID with every message. Previously, I just had
it send a similar thing once, and from that point forward, simply associated
that connection with that user. But this feels more reliable and secure.hj



## Extra client features

Here's the extra stuff that our particular client should do, but isn't required
behavior to work as a client.


### Commands

The client allows you to enter some commands, that send special messages to the
server. This is probably how any client would send those special messages, but
I guess it _could_ be done in any number of ways. In the syntax examples, items
with parentheses are optional.

#### PM
Used to send a private message. Usernames can contain spaces, so you use a colon
to separate the name from the message.
Syntax: `/pm [user]: [message]`.  
Example: `/pm Bob: hey, what's up?`

#### Roll
Used to roll virtual dice. It uses plain old tabletop-style syntax. You can also
use complex rolls, with multiple types of dice. If you add an 'a' or 'd' at the
end, it will roll with advantage or disadvantage, respectively. In this case,
you would otherwise just roll normally, not with two dice.  
Syntax: `/roll [number of dice]d[sides per die](+/-modifier) (...)`  
Example: `/roll 2d20+4`  
Example: `/roll 1d20+1d6+2`  
Example: `/roll 1d8+6`

